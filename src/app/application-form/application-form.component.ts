import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-application-form',
  templateUrl: './application-form.component.html',
  styleUrls: ['./application-form.component.css']
})
export class ApplicationFormComponent implements OnInit {
  application = {
    name : String,
    email: String,
    age : Number,
    phoneNumber: String,
    preffWayOfComm: String,
    englishLevel: String,
    availableToStart: Date,
    tSC : String,
    sPP : String,
    studyFromHome: Boolean

  };

  constructor() { }

  ngOnInit() {
  }

}
